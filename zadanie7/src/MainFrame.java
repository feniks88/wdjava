import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

public class MainFrame extends JFrame {
    private Timer t;
    private Point2D start=new Point2D.Double(5,5),end = new Point2D.Double(100,100);
    public MainFrame(String title){
        super(title);
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension dim = tk.getScreenSize();
        setSize(dim.width/2, dim.height/2);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null); //srodek ekranu
        t = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                start.setLocation(start.getX()+5, start.getY());
                end.setLocation(end.getX()+5,end.getY());
                repaint();
            }
        });

        t.start();

    }

    @Override
    public void paint(Graphics arg0){
        super.paint(arg0);
        Graphics2D g2=(Graphics2D)arg0;
        drawLine(g2,start,end);

        g2.setColor(Color.CYAN);

        Rectangle2D rec = new Rectangle2D.Double(
                100,100,200,300
        );
        g2.draw(rec);

        Ellipse2D el = new Ellipse2D.Double(200,200,300,400);
        g2.draw(el);

    }

    private void drawLine(Graphics2D g2, Point2D start, Point2D end){
        Line2D linia = new Line2D.Double(start,end);
        g2.draw(linia);

    }

}
