package drugie;

import java.awt.*;

public class MainClass {
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                MainFrame mf= new MainFrame("Okno główne");
                mf.setVisible(true);
            }
        });
    }
}
