package drugie;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class MainFrame extends JFrame implements ItemListener {
    private JPanel pImage, pControl;
    private JButton bLoadImage;
    private JLabel lPath, lImage;
    private JFileChooser fch;
    private String path;
    private JComboBox<String> cb = new JComboBox(new String[] {"100%", "50%"});
    private int originalWidth, originalHeight, currentWidth, currentHeight;
    private ImageIcon imageIcon;
    public MainFrame(String arg0){

        super(arg0);
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension dim = tk.getScreenSize();
        setSize(dim.width/2, dim.height/2);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null); //srodek ekranu

        pImage = new JPanel();
        pControl = new JPanel();

        pImage.setBorder(BorderFactory.createRaisedBevelBorder());
        pControl.setBorder(BorderFactory.createRaisedBevelBorder());

        add(pControl, BorderLayout.SOUTH);
        add(pImage, BorderLayout.CENTER);

        bLoadImage = new JButton("Ładuj obrazek");
        pControl.add(bLoadImage);
        lPath = new JLabel("Sciezka do pliku");
        pControl.add(lPath);
        lImage = new JLabel();
        pImage.add(lImage);
        fch = new JFileChooser();

        bLoadImage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JButton buttonClicked = (JButton)e.getSource();
                JPanel p = (JPanel)buttonClicked.getParent();
                Component c = p.getParent();

                if(fch.showOpenDialog(c) == JFileChooser.APPROVE_OPTION){
                    path = fch.getSelectedFile().getAbsolutePath();//zwraca sciezke do wybranego pliku
                    lPath.setText(path);

                    imageIcon = new ImageIcon(path);
                    lImage.setIcon(imageIcon);
                    originalHeight = imageIcon.getIconHeight();
                    originalWidth = imageIcon.getIconWidth();
                }
            }
        });

        pControl.add(cb);
        cb.addItemListener(this);

    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        double scale = 1;
        switch(e.getItem().toString()){
            case "100%":
                scale = 1;
                break;
            case "50%":
                scale = 0.5;
                break;
        }

        currentHeight = (int)(originalHeight * scale);
        currentWidth = (int)(originalWidth * scale);
        Image image = imageIcon.getImage();
        Image rescaledImage = image.getScaledInstance(currentWidth,currentHeight,Image.SCALE_SMOOTH);
        lImage.setIcon(new ImageIcon(rescaledImage));
    }
}
//JButttonGroup - to check, do wybierania opcji, zadanie treningowe