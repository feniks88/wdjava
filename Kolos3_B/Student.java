package Kolos3_B;

public class Student implements Personalia, Administracja, Comparable<Student>{
	protected String Imie, Nazwisko;
	protected int NrGrupy;

	@java.lang.Override
	public java.lang.String toString() {
		return "Student{" +
				"Imie='" + Imie + '\'' +
				", Nazwisko='" + Nazwisko + '\'' +
				", NrGrupy=" + NrGrupy +
				'}';
	}

	public Student(String imie, String nazwisko, int nrGrupy) {
		Imie = imie;
		Nazwisko = nazwisko;
		NrGrupy = nrGrupy;
	}



	@Override
	public String getNazwisko() {
		return this.Nazwisko;
	}

	@Override
	public int compareTo(Student o) {
		return Integer.valueOf(this.getNrGrupy()).compareTo(o.getNrGrupy());
	}

}
