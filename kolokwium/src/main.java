
import java.util.Arrays;
import java.util.Comparator;

public class main {

    public static void main(String[] args) {

        Pojazdy[] poj = {
                new Samochod(125000),
                new Samochod(34900),
                new Motocykl(5000),
                new Samochod(340),
                new Motocykl(50030),

        };

        Arrays.sort(poj, new Comparator<Pojazdy>() {
            @Override
            public int compare(Pojazdy o1, Pojazdy o2) {
                return Integer.valueOf(o1.getPrzebieg()).compareTo(o2.getPrzebieg());
            }
        });

        Arrays.sort(poj, new Comparator<Pojazdy>() {
            @Override
            public int compare(Pojazdy o1, Pojazdy o2) {
                return (o1.getRodzaj()).compareTo(o2.getRodzaj());
            }
        });


        for (Pojazdy p : poj) {
            System.out.println(p.toString());
        }
    }
}