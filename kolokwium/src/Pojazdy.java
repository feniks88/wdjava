import java.util.Objects;

public abstract class Pojazdy {
    protected String rodzaj;
    protected int przebieg;

    public Pojazdy(String rodzaj, int przebieg) {
        this.rodzaj = rodzaj;
        this.przebieg = przebieg;
    }

    public String getRodzaj(){ return this.rodzaj;}

    public int getPrzebieg(){ return this.przebieg;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pojazdy)) return false;
        Pojazdy pojazdy = (Pojazdy) o;
        return Objects.equals(getRodzaj(), pojazdy.getRodzaj());
    }

    @Override
    public String toString() {
        return "Pojazdy{" +
                "rodzaj='" + rodzaj + '\'' +
                ", przebieg=" + przebieg +
                '}';
    }
}
