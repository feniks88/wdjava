import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class KlasaPliki {
    public static void main(String[] args) throws FileNotFoundException {
        //System.out.println(System.getProperty("user.dir"));
        File inFile= new File("inFile.dat");
        Scanner sc = new Scanner(inFile);
        //read
        File outFile= new File("outFile.dat");
        PrintWriter pw = new PrintWriter(outFile);
        //write

        while(sc.hasNext()){
            String tmp = sc.next();
            if(tmp.contains("ala"))
                pw.println(tmp);
        }

        //pw.flush(); //oproznienie bufora
        pw.close();
        sc.close();//zwolnienie zasobow
    }
}
