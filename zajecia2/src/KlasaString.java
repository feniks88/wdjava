public class KlasaString {
    public static void main(String[] args) {
        /*
        //Metoda compareTo()
        String[] tab1 = new String[4];
        tab1[0] = "Jozek";
        tab1[1] = "Wiesiek";
        tab1[2] = "Marian";
        tab1[3] = "Adam";

        String[] tab2 = {"Maryska", "Celina", "Marian", "Zoska"};

        for(int i = 0; i < tab1.length; i++){
            System.out.println(tab1[i].compareTo(tab2[i]));//0 takie same
        }
        */

        //Metoda indexOf(), wyszukuje pierwsze wystapienie, lastIndexOf() ostatnie wystapienie

        String zdanie="Dzisiaj byla ladna pogoda";

        System.out.println(zdanie.indexOf("byla"));
        System.out.println(zdanie.substring(20));

        //Tablica trzech stringow, zawierajaca jakies url, i wycina nazwy plikow

        String[] urls = new String[3];

        urls[0] = "https://facebook.com/plik1.txt";
        urls[1] = "https://google.com/plik2.txt";
        urls[2] = "https://gmail.com/plik3.txt";

        for(int i = 0; i <urls.length; i++){
              int j = urls[i].lastIndexOf("/");
              System.out.println(urls[i].substring(j+1)); //Wyciaganie nazwy plikow
        }

        System.out.println("Niepoprawne adresy");

        for(String url: urls){
            if(!url.contains("https://")) //wypisawnie niepotrawnych adresow
                System.out.println(url);
        }

        StringBuilder sb1 = new StringBuilder("To jest moj string testowy");
        System.out.println(sb1);

        sb1.delete(8,11);
        sb1.insert(8,"twoj");
        System.out.println(sb1);


        
    }
}
