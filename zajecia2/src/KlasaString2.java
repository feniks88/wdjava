public class KlasaString2 {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder(); //String builder
        sb.append("Jozek ma: ");
        sb.append(29);
        sb.append(" lat");
        String s = sb.toString();

        System.out.println(s);

        int i = s.indexOf("29");
        System.out.println(i);

        sb.delete(i,i+2);
        System.out.println(sb);

        sb.insert(i,49);
        System.out.println(sb);
    }
}
