import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class KlasaPlik2 {
    public static void main(String[] args) throws FileNotFoundException {
        //System.out.println(System.getProperty("user.dir"));
        File inFile= new File("inFile2.dat");
        Scanner sc = new Scanner(inFile);
        File outFile= new File("outFile2.dat");
        PrintWriter pw = new PrintWriter(outFile);

        while(sc.hasNext()){
            String tmp = sc.nextLine();
            if(tmp.contains("sub")) {

                int a1 = Integer.parseInt(sc.next());
                int a2 = Integer.parseInt(sc.next());

                int b = a1 - a2;

                pw.println(b);
            }
            else if(tmp.contains("sum")){
                String next = sc.next();
                int a1 = Integer.parseInt(sc.next());
                int a2 = Integer.parseInt(sc.next());

                int b = a1 + a2;

                pw.println(b);

            }
        }

        //pw.flush(); //oproznienie bufora
        pw.close();
        sc.close();//zwolnienie zasobow
    }
}
