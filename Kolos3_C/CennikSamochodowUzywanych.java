package kolo3C;

import java.util.Arrays;
import java.util.Comparator;

public class CennikSamochodowUzywanych {
    private Samochod[] tablica;

    public CennikSamochodowUzywanych(Samochod[] tablica) {
        this.tablica = tablica;
    }

    public void porzadkuj() {
        Arrays.sort(tablica, new Comparator<Samochod>() {
            @Override
            public int compare(Samochod o1, Samochod o2) {
                return Double.compare(o1.getPrzebieg(), o2.getPrzebieg());
            }
        });
    }



    public Samochod[] getTablica() {
        return tablica;
    }
}