package kolo3C;

public class Samochod implements Parametry {

    private double przebieg;
    private String model;

    public Samochod(String model, double przebieg) {
        this.przebieg = przebieg;
        this.model = model;
    }

    @Override
    public double getPrzebieg() {
        return this.przebieg;
    }

    @Override
    public String toString() {
        return "Samochod{" +
                "przebieg=" + przebieg +
                ", model='" + model + '\'' +
                '}';
    }




}
