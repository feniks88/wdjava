package secondProj;

public class MainClass2 {

	public static void main(String[] args) {
		
		final int datasize=4;
		double[][] wspolczynniki=new double[3][datasize];
		
		wspolczynniki[0][0]=2;
		wspolczynniki[1][0]=10;
		wspolczynniki[2][0]=3;
		
		wspolczynniki[0][1]=2;
		wspolczynniki[1][1]=4;
		wspolczynniki[2][1]=2;
		
		wspolczynniki[0][2]=10;
		wspolczynniki[1][2]=2;
		wspolczynniki[2][2]=2;
		
		wspolczynniki[0][3]=-10;
		wspolczynniki[1][3]=10;
		wspolczynniki[2][3]=2;
		
		for(int i=0;i<datasize;i++) {
			double a=wspolczynniki[0][i];
			double b=wspolczynniki[1][i];
			double c=wspolczynniki[2][i];
			System.out.println("Dla wspolczynnikow a= "+a+", b= "+b+", c= "+c);
			
			double delta = Math.pow(b, 2) -4*a*c;
			
			if(delta<0) {
				System.out.println("Nie ma pierwiastkow");
			}
			else {
				double  sqrtdelta=Math.sqrt(delta);
				if(delta>0) {
					
					System.out.println("x1= "+(-b-sqrtdelta)/(2*a)); //konkatenacja stringow, niejawne rozpoznanie metody Double.toString, dzieki klasa Double itd, dziala tez w druga strone
					System.out.println("x2= "+(-b+sqrtdelta)/(2*a));
				}
				else {
					System.out.println("x= "+(-b)/(2*a));
				}
			}
		}
				
				
	}

}
