package secondProj;

public class MainClass {

	public static void main(String[] args) {
		/**final double a = 2; //odpowiednik const z C++, w kontekscie class to znaczy ze nie mozna z niej dziedziczyc
		final double b = 10;
		final double c = 3; **/
		double a=0,b=0,c=0;	
		
		a=Double.parseDouble(args[0]);
		b=Double.parseDouble(args[1]);
		c=Double.parseDouble(args[2]);
		
		
		double delta = Math.pow(b, 2) -4*a*c;
		
		if(delta<0) {
			System.out.println("Nie ma pierwiastkow");
		}
		else {
			double  sqrtdelta=Math.sqrt(delta);
			if(delta>0) {
				
				System.out.println("x1= "+(-b-sqrtdelta)/(2*a)); //konkatenacja stringow, niejawne rozpoznanie metody Double.toString, dzieki klasa Double itd, dziala tez w druga strone
				System.out.println("x2= "+(-b+sqrtdelta)/(2*a));
			}
			else {
				System.out.println("x= "+(-b)/(2*a));
			}
		}
		
	}

}
