package figury;

public class Kolo extends Figura implements Powierzchnie {
	private double promien;

	public Kolo(double promien) {
		super();
		this.promien = promien;
		rodzaj="koło";
	}

	@Override
	public double getPowierzchnia() {
		// TODO Auto-generated method stub
		return Math.PI*Math.pow(promien, 2);
	}

	@Override
	public boolean equals(Object arg0) {
		boolean eq=false;
		if(arg0==null) return false;

		if(this==arg0) return true;

		if(getClass()!=arg0.getClass()) return false;

		Kolo k=(Kolo) arg0;

		if(this.getRodzaj().equals(k.getRodzaj()) && 
				this.promien==k.promien)
			eq=true;
		
		return eq;
	}

}
