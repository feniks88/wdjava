package figury;

public class FiguryMain {

	public static void main(String[] args) {
		Kolo k1=new Kolo(3);
		Kolo k2=new Kolo(3);
		Kolo k3=new Kolo(4);
		Prostokat p1=new Prostokat(2,3);
		System.out.println(k1.equals(k1));
		System.out.println(k1.equals(k2));
		System.out.println(k2.equals(k1));
		System.out.println(k1.equals(k3));
		System.out.println(k1.equals(p1));
		System.out.println(p1.equals(k1));
	}

}
