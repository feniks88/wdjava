package B5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class b5_Main {

    public static void main(String[] args) {
        Set<String> set = new HashSet<>();

        try(Scanner sc = new Scanner(new File("/Users/monika/Documents/wdjava/kolos5/osoby.dat"))){
            while(sc.hasNextLine()){
                String line = sc.nextLine();
                set.add(line);
            }

        } catch (FileNotFoundException e){
            e.printStackTrace();
        }
        System.out.println(set.size());

    }
}
