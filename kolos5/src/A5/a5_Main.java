package A5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class a5_Main {
    public static void main(String[] args) {
        Map<String, Person> personMap = new HashMap<>();

        try(Scanner s = new Scanner(new File("/Users/monika/Documents/wdjava/zajecia8/osoby.dat"))) {
            while(s.hasNextLine()) {
                String linia = s.nextLine();
                StringTokenizer tokenizer = new StringTokenizer(linia);
                String id = tokenizer.nextToken();
                String im = tokenizer.nextToken();
                String na = tokenizer.nextToken();
                Person p = new Person(im, na);
                personMap.put(id, p);
            }

            System.out.println(personMap.get("571")); // pobieranie wartości o konkretnym kluczu
        } catch(FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        }
    }
}

class Person {
    private String im, na;

    public Person(String im, String na) {
        super();
        this.im = im;
        this.na = na;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return Objects.equals(im, person.im) &&
                Objects.equals(na, person.na);
    }

    @Override
    public int hashCode() {
        return Objects.hash(im, na);
    }

    @Override
    public String toString() {
        return "Person{" +
                "im='" + im + '\'' +
                ", na='" + na + '\'' +
                '}';
    }
}

