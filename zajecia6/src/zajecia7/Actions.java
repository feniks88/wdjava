package zajecia7;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class Actions {
    public static class CloseAction extends AbstractAction{
        public CloseAction(){
            putValue(Action.NAME, "Zamknij");
            putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl N"));
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }
}
