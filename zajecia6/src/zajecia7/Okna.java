package zajecia7;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class Okna extends JFrame implements Runnable{
    JButton b1;
    private JPanel panelEdycja, panelPrzyciski, panelDane;
    private JMenuBar pasekMenu;
    private JMenu menuPlik;
    private JMenuItem miClose;
    private JMenu menuPomoc;
    private JButton bClose;

    //Deklaracje akcji
    private Actions.CloseAction closeAction;


    private ArrayList<Czlowiek> listaLudzi;

    private JTable tableLudzie;

    private JScrollPane scrollPane;

    private JTextField tfImie, tfNazwisko, tfDataUr;

    public Okna(String tytul){
        super(tytul);
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();

        setSize(d.width/2,d.height/2);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        //b1 = new JButton("test");
        //add(b1, BorderLayout.NORTH);
        panelEdycja = new JPanel();
        panelPrzyciski = new JPanel();
        panelDane = new JPanel();

        panelEdycja.setBorder(BorderFactory.createRaisedBevelBorder());
        panelPrzyciski.setBorder(BorderFactory.createRaisedBevelBorder());
        panelDane.setBorder(BorderFactory.createRaisedBevelBorder());

        panelEdycja.setPreferredSize(new Dimension(0,80));
        panelPrzyciski.setPreferredSize(new Dimension(100,0));

        add(panelEdycja,BorderLayout.NORTH);
        add(panelPrzyciski,BorderLayout.WEST);
        add(panelDane, BorderLayout.CENTER);

        pasekMenu = new JMenuBar();
        setJMenuBar(pasekMenu);

        closeAction = new Actions.CloseAction();

        menuPlik = new JMenu("Plik");
        miClose = new JMenuItem(closeAction);
        menuPlik.add(miClose);

        menuPomoc = new JMenu("Pomoc");

        pasekMenu.add(menuPlik);
        pasekMenu.add(menuPomoc);

        closeAction = new Actions.CloseAction();
        bClose = new JButton(closeAction);
        panelPrzyciski.add(bClose)  ;

        listaLudzi = new ArrayList<>();
        initList();

        tableLudzie = new JTable(new AbstractTableModel() {
            String[] naglowek = {"Imie", "Nazwisko", "Data urodzenia"} ;

            @Override
            public String getColumnName(int column) {
                return naglowek[column];
            }

            @Override
            public int getRowCount() {
                return listaLudzi.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                String dana=null;
                Czlowiek c = listaLudzi.get(rowIndex);
                switch(columnIndex){
                    case 0: dana = c.getImie(); break;
                    case 1: dana = c.getNazwisko(); break;
                    case 2: dana = c.getDataUrodzenia().toString();

                }
                return dana;
            }
        });

        scrollPane = new JScrollPane(tableLudzie);

        panelDane.add(scrollPane);

        tfImie = new JTextField(15);
        tfNazwisko = new JTextField(30);
        tfDataUr = new JTextField(10);

        panelEdycja.add(tfImie);
        panelEdycja.add(tfNazwisko);
        panelEdycja.add(tfDataUr);

        tfImie.getText();
        tfNazwisko.getText();
        tfDataUr.getText();

    }


    @Override
    public void run() {
            setVisible(true);

    }
    
    private void initList(){
        listaLudzi.add(new Czlowiek("Jan", "Kowalski", LocalDate.of(1995,01,01)));
        listaLudzi.add(new Czlowiek("Dzban", "Kot", LocalDate.of(1996,01,01)));
        listaLudzi.add(new Czlowiek("Krzysiek", "DSADA", LocalDate.of(1997,01,01)));
        listaLudzi.add(new Czlowiek("Ola", "Młot", LocalDate.of(1998,01,01)));

    }
}
