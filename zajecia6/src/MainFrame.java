import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;


public class MainFrame extends JFrame implements Runnable, ActionListener, ItemListener {


    private JButton b1;
    private JButton b2;
    private JButton b3;
    private JPanel contentPanel;
    private JComboBox<String> cb;
    private String[] position = {"Monday","Tuesday", "Wednesday", "Thursday", "Friday"} ;
    private JLabel label;
    public MainFrame(String title){
        super(title);
        setSize(400,250);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        contentPanel = new JPanel();
        setContentPane(contentPanel);

        b1 = new JButton("Przycisk1");
        add(b1);
        ButtonClickListener listener = new ButtonClickListener();
        b1.addActionListener(listener);

        b2 = new JButton("Przycisk2");
        add(b2);
        b2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JButton b = (JButton)e.getSource();
                JOptionPane.showMessageDialog(b.getParent(),"Klieknieto przycisk ");

            }
        });

        b3 = new JButton("Przycisk3");
        add(b3);
        b3.addActionListener(this);
        cb = new JComboBox<String>(position);
        add(cb);
        cb.addItemListener(this);
        label = new JLabel();
        add(label);
    }
    @Override
    public void run() {
        setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
             JButton b = (JButton)e.getSource();                                                                                                             
                                                JOptionPane.showMessageDialog(null,"Klieknieto przycisk "+b.getText());   }

    @Override
    public void itemStateChanged(ItemEvent e) {
        label.setText(e.getItem().toString());
    }

    class ButtonClickListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            JButton b = (JButton)e.getSource();

            JOptionPane.showMessageDialog(null,"Klieknieto przycisk "+b.getText());
        }
    }

}
