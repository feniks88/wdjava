package Pracownik;

public class Kierownik extends Pracownik{
    double dodatek = 200;
    public Kierownik(String imie_nazwisko) {
        super(imie_nazwisko);
    }

    @Override
    public double getPensja() {
        return pensja + dodatek;
    }

    @Override
    public double zwiekszWynagrodzenie(double procent) {
        return super.zwiekszWynagrodzenie(procent);
    }
}
