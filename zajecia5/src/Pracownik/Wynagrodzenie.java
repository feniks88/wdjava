package Pracownik;

public interface Wynagrodzenie {
    public double getPensja();
    public double zwiekszWynagrodzenie(double procent);

}
