package Pracownik;

public class Pracownik implements Wynagrodzenie{
    protected String imie_nazwisko;
    protected double pensja = 3000;

    public Pracownik(String imie_nazwisko){
        this.imie_nazwisko = imie_nazwisko;
    }


    @Override
    public double getPensja() {
        return pensja;
    }

    @Override
    public double zwiekszWynagrodzenie(double procent) {
        return pensja+(pensja*(procent/100.));
    }
}
