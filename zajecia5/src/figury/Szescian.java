package figury;

public class Szescian extends Bryla {

    private double bok;

    public Szescian(double bok) {
        super();
        this.bok = bok;
        rodzaj = "szescian";
    }


    @Override
    public double getObjetosc() {
        return 6*bok;
    }

    @Override
    public double getPowierzchnia() {
        return 6*bok;
    }
}
