package figury;

public abstract class Bryla implements Rodzaje, Powierzchnie, Objetosci, Comparable<Bryla> {
    protected String rodzaj;
    @Override
    public String getRodzaje() {
        return rodzaj;
    }

    @Override
    public int compareTo(Bryla bryla) {

        return Double.valueOf(this.getObjetosc()).compareTo(bryla.getObjetosc());
    }
}
