package figury;

import java.util.Arrays;
import java.util.Comparator;

public class BrylyMain {
    public static void main(String[] args) {

        class PowierzchnieKomparator implements Comparator<Bryla> {

            @Override
            public int compare(Bryla o1, Bryla o2) {
                return Double.valueOf(o1.getPowierzchnia()).compareTo(o1.getPowierzchnia());

            }
        }

        Bryla[] bryly = new Bryla[5];
        bryly[0] = new Kula(2);
        bryly[1] = new Szescian(2);
        bryly[2] = new Kula(4);
        bryly[3] = new Szescian(4);
        bryly[4] = new Kula(6);

        for(Bryla b: bryly){
            System.out.println(b.getRodzaje() + " " + b.getPowierzchnia() + " " + b.getObjetosc());
        }

        Arrays.sort(bryly);

        System.out.println("---Po objetosci----");

        for(Bryla b: bryly){
            System.out.println(b.getRodzaje() + " " + b.getPowierzchnia() + " " + b.getObjetosc());
        }

        Arrays.sort(bryly, new PowierzchnieKomparator());

        System.out.println("---Po powierzchni----");

        for(Bryla b: bryly){
            System.out.println(b.getRodzaje() + " " + b.getPowierzchnia() + " " + b.getObjetosc());
        }

        Arrays.sort(bryly, new Comparator<Bryla>() {
            @Override
            public int compare(Bryla o1, Bryla o2) {
                return (o1.getRodzaje()).compareTo(o2.getRodzaje());
            }
        }); //klasa anonimowa, wyrazenie lambda

        System.out.println("---Alfabetycznie----");

        for(Bryla b: bryly){
            System.out.println(b.getRodzaje() + " " + b.getPowierzchnia() + " " + b.getObjetosc());
        }

    }
}

