package figury;

public class Kolo extends Figura implements Powierzchnie {
    private double promien;

    public Kolo(double promien) {
        super();
        this.promien = promien;
        rodzaj= "kolo";
    }

    @Override
    public double getPowierzchnia() {
        return Math.PI*Math.pow(promien,2);

        /*
        Object o = new Object();
        o.equals(); porownywanie stanow obiektow
        */
    }
}
