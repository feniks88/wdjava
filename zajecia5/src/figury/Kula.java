package figury;

public class Kula extends Bryla {
    private double promien;

    public Kula(double promien) {
        super();
        this.promien = promien;
        rodzaj = "kula";
    }


    @Override
    public double getObjetosc() {
        return 4./3.*Math.PI*Math.pow(promien,3);
    }

    @Override
    public double getPowierzchnia() {
        return 4*Math.PI*Math.pow(promien,2);
    }
}
