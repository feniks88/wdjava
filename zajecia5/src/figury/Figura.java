package figury;

import java.util.Objects;

public class Figura implements Rodzaje {

    protected String rodzaj;


    @Override
    public int hashCode() {
        return Objects.hash(rodzaj);
    }

    @Override
    public String getRodzaje() {
        return rodzaj;
    }
}
