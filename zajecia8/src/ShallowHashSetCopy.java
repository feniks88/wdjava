import java.time.LocalDate;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

public class ShallowHashSetCopy {

	public static void main(String[] args) {
		Set<Czlowiek> ludzie1=new HashSet<>();
		ludzie1.add(new Czlowiek("Maria", LocalDate.of(2000, 1, 1)));
		ludzie1.add(new Czlowiek("Zofia", LocalDate.of(2000, 1, 1)));
		
		ludzie1.forEach(System.out::println);
		
		Set<Czlowiek> ludzie2=new HashSet<>(ludzie1);
		ludzie2.forEach(System.out::println);
		
		System.out.println("--------------------");
		
		Iterator<Czlowiek> i1=ludzie1.iterator();
		while(i1.hasNext())
			i1.next().setDate(LocalDate.of(2001, 1, 1));
		
		ludzie1.forEach(System.out::println);
		ludzie2.forEach(System.out::println);
	}

}

class Czlowiek{
	String imie;
	LocalDate dataUr;
	public Czlowiek(String imie, LocalDate dataUr) {
		super();
		this.imie = imie;
		this.dataUr = dataUr;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return imie+" "+dataUr.toString();
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return Objects.hash(imie,dataUr);
	}
	
	@Override
	public boolean equals(Object arg0) {
		if(arg0==null) return false;
		if(arg0==this) return true;
		if(!arg0.getClass().equals(getClass())) return false;
		
		Czlowiek o=(Czlowiek) arg0;
		return this.imie.equals(o.imie) && this.dataUr.equals(o.dataUr);
	}
	
	public void setDate(LocalDate dataUr) {
		this.dataUr=dataUr;
	}
}
