package Homework;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Homework {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        int size = 0;

        try(Scanner sc = new Scanner(new File("/Users/monika/Documents/wdjava/zajecia8/src/Homework/Pan Tadeusz.txt"))){
            while(sc.hasNextLine()){
                String line = sc.nextLine();
                if(!line.isEmpty()) {
                    line = line.toLowerCase();
                    line = line.replaceAll("[^a-z]", " ");
                    line = line.replaceAll("( +)", " ").trim();
                    String[] words = line.split("\\s");
                    for (int i = 0; i < words.length; i++) {
                        if (set.add(words[i]))
                            size++;
                    }
                }
            }


        } catch (FileNotFoundException e){ //wyjatek to sytuacja ktora generuje blad ale nie mozna jej wykluczyc
            e.printStackTrace();
        }
        System.out.println(size);

    }
}
