package lambdas;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class FileAnalysis {
    public static void main(String[] args) {
        System.out.println(System.getProperty("user.home"));
        File f = new File(System.getProperty("user.home") + "\\Dysk_Google");
        File[] files = f.listFiles();
        List<File> fileList = Arrays.asList(files);
        fileList.forEach(System.out::println);
        fileList.stream().filter(x -> x.isFile()).forEach(System.out::println); //zwroci liste samych plikow
        fileList.stream().filter(x -> x.isFile()).mapToInt(x -> (int)x.length()).forEach(System.out::println); //zwraca wielkosc plikow
        System.out.println(fileList.stream().filter(x -> x.isFile()).mapToInt(x -> (int)x.length()).sum());//zwraca sume wielkosci
    }
}
