package lambdas;

public class StringPresenter {
    private static String [] tab = {"sdasad", "dweterhhn", "fdgdf" , "skdgkgogogo", "okoko"};
    public static void main(String[] args) {
        showStrings(x -> x);
        showStrings(x -> x.toUpperCase());
        showStrings(x -> x.substring(0,5));
    }

    public static void showStrings(StringManipulator sm){

        for(int i = 0; i < tab.length; i++){
            System.out.println(sm.mainpulate(tab[i]));
        }
    }
}

interface StringManipulator{
    String mainpulate(String a);
}