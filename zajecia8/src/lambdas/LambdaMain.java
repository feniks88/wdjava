package lambdas;

public class LambdaMain {
    public static void main(String[] args) throws NotSameSizeArrayException {

        int[] a = {1,2,3,4,5};
        int[] b = {2,3,4,5,6};
        ArrayPresenter ap = new ArrayPresenter(a,b);
//        ap.showResults(new IntegegerMainipulator() {
//            @Override
//            public int manipulate(int a, int b) {
//                return a+b;
//            }
//        });
        ap.showResults((x,y) -> x+y);
        ap.showResults((x,y) -> x*y); //wyrazenie lambda
        ap.showResults((x,y) -> (x+y)*10);
    }
}

class ArrayPresenter{
    private int[] tab1, tab2;

    public ArrayPresenter(int[] tab1, int[] tab2) throws NotSameSizeArrayException{
        if(tab1.length != tab2.length ) throw new NotSameSizeArrayException("Tablice nie maja tego samego rozmiaru");
        this.tab1 = tab1;
        this.tab2 = tab2;
    }

    public void showResults(IntegegerMainipulator im){
        for(int i = 0; i < tab1.length; i++){
            System.out.println(im.manipulate(tab1[i], tab2[i]));
        }
    }
}

@FunctionalInterface
interface IntegegerMainipulator{
    int manipulate(int a, int b);
}

class NotSameSizeArrayException extends Exception{
    public NotSameSizeArrayException(String message) {
        super(message);
    }
}