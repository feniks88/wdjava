import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class ListMain {
    public static void main(String[] args) {
        List<String> lista = new LinkedList<>();
        lista.add("Wiesiek");
        lista.add("Czesiek");
        lista.add("Krzysiek");

        for(String s: lista )
            System.out.println(s);

        Iterator<String> it = lista.iterator();
        while(it.hasNext())
            System.out.println(it.next());

        ListIterator<String> it1 = lista.listIterator();
        it1.next();
        it1.add("Zenek");

        ListIterator<String> it2 = lista.listIterator();
        it1.next();
        it1.next();
        it1.remove();

        List<String> lista2= new LinkedList<>();
        lista2.add("Monika");
        lista2.add("Czesiek");
        lista2.add("Krzysiek");
        lista2.add("Czesiek");
        lista2.add("Krzysiek");
        ListIterator<String> iter2 = lista2.listIterator();

        List<Integer> lista3= new LinkedList<>();
        lista3.add(1);
        lista3.add(2);
        lista3.add(3);
        ListIterator<Integer> iter3 = lista3.listIterator();

        while(iter3.hasNext()){
            iter2.next();
            iter2.add(iter3.next().toString());

        }
        while(iter2.hasNext()){
            iter2.next();
            iter2.remove();
        }

        for(String s: lista2 )
            System.out.println(s);
    }
}
