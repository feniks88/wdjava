import java.time.LocalDate;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

public class DeepHashSetCopy {

	public static void main(String[] args) throws CloneNotSupportedException {
		Set<Czlowieczek> ludzie1=new HashSet<>();
		ludzie1.add(new Czlowieczek("Maria", LocalDate.of(2000, 1, 1)));
		ludzie1.add(new Czlowieczek("Zofia", LocalDate.of(2000, 1, 1)));
		
		ludzie1.forEach(System.out::println);
		
		Set<Czlowieczek> ludzie2=new HashSet<>();
		Iterator<Czlowieczek> i2=ludzie1.iterator();
		while(i2.hasNext())
			ludzie2.add((Czlowieczek)i2.next().clone());
		
		ludzie2.forEach(System.out::println);
		
		System.out.println("--------------------");
		
		Iterator<Czlowieczek> it3=ludzie1.iterator();
		while(it3.hasNext())
			it3.next().setDate(LocalDate.of(2001, 1, 1));
		ludzie1.forEach(System.out::println);
		ludzie2.forEach(System.out::println);
	}

}

class Czlowieczek implements Cloneable{
	String imie;
	LocalDate dataUr;
	public Czlowieczek(String imie, LocalDate dataUr) {
		super();
		this.imie = imie;
		this.dataUr = dataUr;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return imie+" "+dataUr.toString();
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return Objects.hash(imie,dataUr);
	}
	
	@Override
	public boolean equals(Object arg0) {
		if(arg0==null) return false;
		if(arg0==this) return true;
		if(!arg0.getClass().equals(getClass())) return false;
		
		Czlowieczek o=(Czlowieczek) arg0;
		return this.imie.equals(o.imie) && this.dataUr.equals(o.dataUr);
	}
	
	public void setDate(LocalDate dataUr) {
		this.dataUr=dataUr;
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
}
