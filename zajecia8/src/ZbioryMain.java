import java.util.HashSet;
import java.util.Set;

public class ZbioryMain {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("Romek");
        set.add("Grażyna");
        set.add("Janusz"); //zwracan TRUE jak dodał się element, nie ma duplikatów

        for(String s: set)
            System.out.println(s);
    }
}

//Zadanie domowe, załadować pana tadeusza i policzyć unikalne wyrazy