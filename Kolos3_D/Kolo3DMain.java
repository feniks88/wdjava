package kolo3D;

public class Kolo3DMain {
    public static void main(String[] args) {
        Figura[] figury = {
                new Kolo("zielony", 20),
                new Kolo("czerwony", 40),
                new Kwadrat("zielony", 20),
                new Kwadrat("czerwony", 40),
                new Kwadrat("zielony", 80)
        };

        Figura wzorzec = new Kwadrat("zielony", 20);

        for(Figura f: figury) {
            if(f.equals(wzorzec)) {
                System.out.println(f.toString());
            }
        }
    }
}
