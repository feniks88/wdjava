package kolo3D;

import java.util.Objects;

public class Figura implements Atrybuty, Powierzchnia {
    private String rodzaj, kolor;
    private double powierzchnia;

    public Figura(String rodzaj, String kolor, double powierzchnia) {
        this.rodzaj = rodzaj;
        this.kolor = kolor;
        this.powierzchnia = powierzchnia;
    }

    @Override
    public String getRodzaj() {
        return this.rodzaj;
    }

    @Override
    public String getKolor() {
        return this.kolor;
    }

    @Override
    public double getPowierzchnia() {
        return this.powierzchnia;
    }

    @Override
    public String toString() {
        return "Figura{" +
                "rodzaj='" + rodzaj + '\'' +
                ", kolor='" + kolor + '\'' +
                ", powierzchnia=" + powierzchnia +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Figura)) return false;
        Figura figura = (Figura) o;
        return Objects.equals(getRodzaj(), figura.getRodzaj()) &&
                Objects.equals(getKolor(), figura.getKolor());
    }

}