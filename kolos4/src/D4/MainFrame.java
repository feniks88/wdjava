package D4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

public class MainFrame extends JFrame {

    private JPanel panelControl;
    private MyPanel panelDraw;
    private JTextField txtBeginX, txtBeginY, txtEndX, txtEndY;
    private JComboBox<String> cmbColors;
    private JButton btnDraw;

    private Color color = Color.BLUE;
    private Line2D linia = null;

    public MainFrame(String title) throws HeadlessException {
        super(title);

        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(screen.width / 2, screen.height / 2);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        createComponents();
        addListeners();
    }

    public void createComponents() {
        txtBeginX = new JTextField(5);
        txtBeginY = new JTextField(5);
        txtEndX = new JTextField(5);
        txtEndY = new JTextField(5);

        cmbColors = new JComboBox<>(new String[]{"niebieski", "czerwony"});

        btnDraw = new JButton("Rysuj");

        panelControl = new JPanel();
        panelControl.setPreferredSize(new Dimension(0,100));
        panelControl.add(txtBeginX);
        panelControl.add(txtBeginY);
        panelControl.add(txtEndX);
        panelControl.add(txtEndY);
        panelControl.add(cmbColors);
        panelControl.add(btnDraw);

        panelDraw = new MyPanel();

        add(panelControl, BorderLayout.SOUTH);
        add(panelDraw, BorderLayout.CENTER);
    }

    public void addListeners() {
        cmbColors.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                switch(e.getItem().toString()) {
                    case "niebieski":
                        color = Color.BLUE;
                        break;
                    case "czerwony":
                        color = Color.RED;
                        break;
                    default:
                        color = Color.BLACK;
                }
            }
        });

        btnDraw.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double sX, sY, eX, eY;
                Point2D start, end;
                sX = Double.parseDouble(txtBeginX.getText());
                sY = Double.parseDouble(txtBeginY.getText());
                eX = Double.parseDouble(txtEndX.getText());
                eY = Double.parseDouble(txtEndY.getText());
                start = new Point2D.Double(sX, sY);
                end  = new Point2D.Double(eX, eY);
                linia = new Line2D.Double(start, end);
                repaint();
            }
        });
    }

    class MyPanel extends JPanel {
        public MyPanel() {
            super(true);
        }

        @Override
        public void paint(Graphics g) {
            super.paint(g);
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(color);
            if(linia != null) g2.draw(linia);
        }
    }
}