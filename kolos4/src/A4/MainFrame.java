package A4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainFrame extends JFrame {

    private JPanel panelMain; //glowny panel
    private JTextField txtField1, txtField2; //Pola edycyjne, do wpisywania wartosc
    private JComboBox<String> cmbOperations; //Lista rozwijana do Akcji
    private JLabel lblResult; //Do wyswietlenia rezultatu
    private JButton btnCalc; //Przycisk

    public MainFrame(String title) throws HeadlessException {
        super(title);

        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(screen.width / 2, screen.height / 2);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        createComponents();
        addListeners();
    }

    public void createComponents() {
        panelMain = new JPanel();
        //Pola edycyjne
        txtField1 = new JTextField(10);
        txtField2 = new JTextField(10);
        //Lista rozwijana
        cmbOperations = new JComboBox<>(new String[] {"plus", "minus", "razy"});
        //Do wyswietlania rezultatu
        lblResult = new JLabel("Rezultat");
        //przycisk
        btnCalc = new JButton("Oblicz");

        add(panelMain);

        panelMain.add(txtField1);
        panelMain.add(txtField2);
        panelMain.add(cmbOperations);
        panelMain.add(btnCalc);
        panelMain.add(lblResult);
    }

    public void addListeners() {
        btnCalc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double data1, data2;
                String operation;
                data1 = Double.parseDouble(txtField1.getText()); //pobieranie tekstu z pola edycyjnego
                data2 = Double.parseDouble(txtField2.getText());
                operation = cmbOperations.getSelectedItem().toString(); //pobieranie wybranej opcji
                switch(operation) {
                    case "plus":
                        lblResult.setText("" + (data1 + data2)); //ustawianie rezultatu
                        break;
                    case "minus":
                        lblResult.setText("" + (data1 - data2));
                        break;
                    case "razy":
                        lblResult.setText("" + (data1 * data2));
                        break;
                    default:
                        lblResult.setText("Nieprawidłowa operacja");
                }
            }
        });
    }
}
