package B4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;

public class MainFrame extends JFrame {

    private JPanel panelMain;
    private MyPanel panelDraw; //panel do rysowania
    private JComboBox<String> cmbColors; //Lista rozwijana
    private JButton btnDraw; // przycisk
    private Color color = Color.BLUE; // wybrany kolor


    public MainFrame(String title) throws HeadlessException {
        super(title);

        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(screen.width / 2, screen.height / 2);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        createComponents();
        addListeners();
    }

    public void createComponents() {
        panelMain = new JPanel();
        panelMain.setPreferredSize(new Dimension(0, 100));
        panelDraw = new MyPanel();
        cmbColors = new JComboBox<>(new String[]{"niebieski", "zielony", "czerwony"});
        btnDraw = new JButton("Rysuj");

        add(panelMain, BorderLayout.SOUTH);
        add(panelDraw, BorderLayout.CENTER);
        panelMain.add(cmbColors);
        panelMain.add(btnDraw);
    }

    public void addListeners() {
        btnDraw.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String selectedColor = cmbColors.getSelectedItem().toString();
                switch(selectedColor) {
                    case "niebieski":
                        color = Color.BLUE;
                        break;
                    case "zielony":
                        color = Color.GREEN;
                        break;
                    case "czerwony":
                        color = Color.RED;
                        break;
                    default:
                        color = Color.BLACK;
                }
                repaint(); //rysowanie okna, wywyłuje paint z klasy
            }
        });
    }

    class MyPanel extends JPanel { //klasa do okienka
        public MyPanel() {
            super(true);
        }

        @Override
        public void paint(Graphics g) { // metoda do rysowania
            super.paint(g);
            Graphics2D g2 = (Graphics2D) g;
            Rectangle2D rect = new Rectangle2D.Double(50,50,200,200);
            g2.setColor(color);
            g2.draw(rect);
        }
    }
}