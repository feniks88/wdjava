package C4;

import java.awt.*;

// Timer przesuwa koło, ItemListener łapie zmiane koloru

public class MainClass {
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainFrame("kolo4C").setVisible(true);
            }
        });
    }
}