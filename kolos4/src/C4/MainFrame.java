package C4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.geom.Ellipse2D;
public class MainFrame extends JFrame {

    private JPanel panelMain;
    private MyPanel panelDraw;
    private JComboBox<String> cmbColors;
    private JButton btnDraw;
    private Color color = Color.BLUE;
    protected Ellipse2D elipsa = null; // Żeby na początku się nic nie rysowało
    protected Timer timer;
    private int iterations = 0;

    public MainFrame(String title) throws HeadlessException {
        super(title);

        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(screen.width / 2, screen.height /2);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        createComponents();
        addListeners();
    }

    public void createComponents() {
        panelMain = new JPanel();
        panelDraw = new MyPanel();
        cmbColors = new JComboBox<>(new String[]{"niebieski", "zielony", "czerwony"});
        btnDraw = new JButton("Rysuj");


        panelMain.add(cmbColors);
        panelMain.add(btnDraw);
        panelMain.setPreferredSize(new Dimension(0,100));
        add(panelMain, BorderLayout.SOUTH);
        add(panelDraw, BorderLayout.CENTER);
    }

    public void addListeners() {
        cmbColors.addItemListener(new ItemListener() { // nasłuchiwanie zmiany koloru
            @Override
            public void itemStateChanged(ItemEvent e) {
                switch (e.getItem().toString()) {
                    case "niebieski":
                        color = Color.BLUE;
                        break;
                    case "zielony":
                        color = Color.GREEN;
                        break;
                    case "czerwony":
                        color = Color.RED;
                        break;
                    default:
                        color = Color.BLACK;
                }
            }
        });

        timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                elipsa = new Ellipse2D.Double(50 + (iterations * 10), 50, 200, 200);
                iterations++;
                if(iterations >= 20) timer.stop();
                repaint();

            }
        } );

        btnDraw.addActionListener(new ActionListener() { //uruchomienie odliczania
            @Override
            public void actionPerformed(ActionEvent e) {
                iterations = 0;
                timer.start();
            }
        });
    }

    class MyPanel extends JPanel {
        public MyPanel() {
            super(true);
        }

        @Override
        public void paint(Graphics g) {
            super.paint(g);
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(color);
            if(elipsa != null) g2.draw(elipsa);
        }
    }
}
