package F4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainFrame extends JFrame {

    private JPanel panelMain;
    private JTextField txtField1, txtField2;
    private CopyAction copyAction;
    private JMenuItem miCopy;

    public MainFrame(String title) throws HeadlessException {
        super(title);

        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(screen.width / 2, screen.height / 2);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        createComponents();
    }

    public void createComponents() {
        panelMain = new JPanel();

        txtField1 = new JTextField(10);
        txtField2 = new JTextField(10);


        add(panelMain);

        panelMain.add(txtField1);
        panelMain.add(txtField2);


        copyAction = new CopyAction();
        miCopy = new JMenuItem(copyAction);
        panelMain.add(miCopy);


    }

    public class CopyAction extends AbstractAction{
        public CopyAction(){
            putValue(Action.NAME, "Kopiuj");
            putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl C"));
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            String data1;
            data1 = txtField1.getText();
            txtField2.setText(data1);
        }
    }


}

