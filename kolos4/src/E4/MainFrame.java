package E4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainFrame extends JFrame {

    private JPanel panelMain;
    private JTextField txtField1;
    private JButton btnCalc;

    public MainFrame(String title) throws HeadlessException {
        super(title);

        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(screen.width / 2, screen.height / 2);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        createComponents();
        //addListeners();
    }

    public void createComponents() {
        panelMain = new JPanel();

        //dodawanie przycisków i paneli np.
        txtField1 = new JTextField(10);

        add(panelMain);

        //dodawanie tego do głównego panelu
        panelMain.add(txtField1);

    }

    public void addListeners() {
        btnCalc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
    }
}