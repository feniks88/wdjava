package E4;

import java.awt.*;

public class MainClass {
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainFrame("kolos4E").setVisible(true);
            }
        });
    }
}
