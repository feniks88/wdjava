package Pojazd;

public class Osobowy extends Pojazd{

    private String typ;
    int ilosc_drzwi;

    public Osobowy(String nrRej, String vin, String kolor, float cena, float spalanie) {
        super(nrRej, vin, kolor, cena, spalanie);
        typ = "osobowy";
        ilosc_drzwi = 4;
    }

    @Override
    public String getType() {
        return typ;
    }
}
