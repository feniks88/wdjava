package Pojazd;

public abstract class Pojazd {
    private String nrRej, Vin, kolor;
    private float cena, km, stanZbiornika, spalanie;

    public abstract String getType();


    public Pojazd(String nrRej, String vin, String kolor, float cena, float spalanie) {
        this.nrRej = nrRej;
        Vin = vin;
        this.kolor = kolor;
        this.cena = cena;
        km = 0;
        stanZbiornika = 0;
        this.spalanie = spalanie;
    }

    public void jedz(float ile){

        if(stanZbiornika == 0 ){
            System.out.println("Brak paliwa");
        }
        else{
            float potrzebnePaliwo = (spalanie*ile) / 100;
            if(potrzebnePaliwo > stanZbiornika){
                float max = (100*stanZbiornika)/spalanie;
                jedz(max);
            }
            else{
                stanZbiornika -= potrzebnePaliwo;
                km+=ile;
                System.out.println("Przejechano " + ile + " km");
            }

        }

    }

    public void tankuj(float ile){
        stanZbiornika += ile;
    }

    @Override
    public String toString(){
        return "Pojazd to: " + getType() +  " za " + cena + " zł, o kolorze " + kolor;
    }

    public void getPaliwo(){
        System.out.println(stanZbiornika);
    }
}
