package Pojazd;

public class Motocykl extends Pojazd {

   private String typ;

    public Motocykl(String nrRej, String vin, String kolor, float cena, float spalanie) {
        super(nrRej, vin, kolor, cena, spalanie);
        typ = "motocykl";
    }

    @Override
    public String getType() {
        return typ;
    }
}
