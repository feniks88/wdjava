package Pojazd;

import java.util.ArrayList;
import java.util.List;

public class MainClass {
    public static void main(String[] args) {
        List<Pojazd> pojazdList = new ArrayList<>();

        pojazdList.add(new Osobowy("DB1234","456","czerowy", 400, 10 ));
        pojazdList.add(new Osobowy("KRK129","90386","bialy", 400, 7 ));
        pojazdList.add(new Motocykl("KRK23", "098", "czarny", 500,5));

        for( Pojazd p: pojazdList){
            System.out.println(p.toString());
        }

        pojazdList.get(0).tankuj(8);
        pojazdList.get(0).getPaliwo();
        pojazdList.get(0).jedz(100);
        pojazdList.get(0).getPaliwo();

    }
}
