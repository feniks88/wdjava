import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {

        int suma = 0;
        File inFile = new File("F.dat");

        Scanner sc = new Scanner(inFile);
        while(sc.hasNext()){
            String next = sc.next();
            String next2 = sc.next();
            int data = Integer.parseInt(sc.next());
            suma += data;
        }
        sc.close();

        System.out.println(suma);

    }
}
