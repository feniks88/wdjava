package zwierze;

public class Kot extends Zwierze {

    public Kot() {
        super("kot");
    }

    @Override
    public void dajGlos() {
        System.out.println("Miau");
    }
}
