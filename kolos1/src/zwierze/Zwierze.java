package zwierze;

public abstract class Zwierze {
    private String rodzaj;


    public Zwierze(String rodzaj) {
        this.rodzaj = rodzaj;
    }

    public String getRodzaj(){
        return rodzaj;
    }

    public abstract void dajGlos();

    public void mow(int ile){
        for(int i = 0; i < ile; i++){
           dajGlos();
        }
    }
}
