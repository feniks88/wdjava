import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TryWithResources {

    public static void main(String[] args) {

        try(Scanner sc = new Scanner(new File("input.dat"))){

            while(sc.hasNextLine()){
                System.out.println(sc.nextLine());
            }


        } catch (FileNotFoundException e){ //wyjatek to sytuacja ktora generuje blad ale nie mozna jej wykluczyc
            e.printStackTrace();
        }

    }
}
