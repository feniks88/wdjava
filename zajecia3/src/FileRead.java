import java.io.File;
import java.io.FileNotFoundException;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class FileRead {

    public static void main(String[] args) {
        try {
            readFile("input.txt");
        } catch (BirthYearNotProvidedException e) {

            System.out.println(e.getMessage());
        }

    }


    public static void readFile(String path) throws BirthYearNotProvidedException {
        try (Scanner sc = new Scanner(new File(path))) {
            while (sc.hasNextLine()) {
                String linia = sc.nextLine();
                StringTokenizer st = new StringTokenizer(linia);
                String name = st.nextToken();
                String surrname = st.nextToken();
                try {
                    int year = Integer.parseInt(st.nextToken());
                    System.out.println(name + " " + surrname + " " + year);
                } catch (NoSuchElementException e) {
                    throw new BirthYearNotProvidedException();
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }
}

class BirthYearNotProvidedException extends Exception {
    public BirthYearNotProvidedException() {
        super("Nie podano roku urodzenia");
    }
}
