import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Iloraz2 {
    public static void main(String[] args) {

        readFile("input2.txt");

    }

    public static void readFile(String path) {
        try (Scanner sc = new Scanner(new File(path))) {
            boolean tmp = true;
            while (tmp) if (!sc.hasNextLine()) {
                EndOfFiles e = new EndOfFiles();
                System.out.println(e.getMessage());
                tmp = false;

            } else {
                String linia = sc.nextLine();
                StringTokenizer st = new StringTokenizer(linia);
                float a = Integer.parseInt(st.nextToken());
                float b = Integer.parseInt(st.nextToken());
                if (b == 0) {

                    Divide e = new Divide();
                    System.out.println(e.getMessage());
                } else {
                    System.out.println(a / b);
                }

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}

class EndOfFiles extends Exception {
    public EndOfFiles() {
        super("Koniec pliku");
    }

}

class Divide extends Exception {
    public Divide() {
        super("Nie można dzielic prze zero");
    }

}
