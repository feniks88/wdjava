import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class Scanner1 {

    public static void main(String[] args) {
        Scanner sc = null;
        try{
            sc = new Scanner(new File("input.dat"));
            while(sc.hasNextLine()){
                System.out.println(sc.nextLine());
            }



        }
        catch (FileNotFoundException e){
            //e.printStackTrace();
            System.out.println("Niepoprawna nazwa pliku");
        }
        finally {
            sc.close();
        }

    }
}
