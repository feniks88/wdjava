public class LapanieWyjatkow {
    static void obliczOdwrotnosc(double []tab){
       try{ for(int i = 0; i< tab.length; i++){
            tab[i] = 1 / tab[i];
        }}
       catch(NullPointerException e){
           System.out.println("Przekazana nieutworzona tablica");
       }
       catch(ArithmeticException e){
           System.out.println("Nie można dzielic przez zero");
       }
       catch(Exception e){
           System.out.println("Nieprzewidziany wyjatek");
       }

    }
    public static void main(String[] args){
        double tab[] = {1,0,3};
        obliczOdwrotnosc(tab);
        obliczOdwrotnosc(null);

    }
}
