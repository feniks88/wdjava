package Kolokwium;

public class Motocykl extends Pojazd{
    public Motocykl(int przebieg, int liczbaPasazerow) throws TooManyPassangersException {
        if(liczbaPasazerow>2)
            throw new TooManyPassangersException());
        super("motocykl", przebieg, liczbaPasazerow);
    }

    @Override
    public String getRodzaj() {
        return "motocykl";
    }
}
