package Kolokwium;

public class Samochod extends Pojazd {


    public Samochod(int przebieg, int liczbaPasazerow) throws TooManyPassangersException {
        if(liczbaPasazerow>5)
            throw new TooManyPassangersException();
        super("samochod", przebieg, liczbaPasazerow);
    }

    @Override
    public String getRodzaj() {
        return "Samochod";
    }


}
