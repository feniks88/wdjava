package Kolokwium;

public abstract class Pojazd {
    private String Rodzaj;
    private int przebieg;
    private int LiczbaPasazerow;

    public Pojazd(String rodzaj, int przebieg, int liczbaPasazerow){
        Rodzaj = rodzaj;
        this.przebieg = przebieg;
        LiczbaPasazerow = liczbaPasazerow;
    }

    public abstract String getRodzaj();

    @Override
    public String toString(){
        return Rodzaj + przebieg + LiczbaPasazerow;
    }
}

}
