public class prostokat  {
    private double szerokosc, wysokosc;

    public prostokat (double szerokosc, double wysokosc){
        this.szerokosc=szerokosc;
        this.wysokosc=wysokosc;
    }
    public double getPole(){
        return szerokosc*wysokosc;
    }
    public double getObwod(){
        return 2*(szerokosc +wysokosc);
    }
}
