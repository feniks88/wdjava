package Czlowiek;

import java.time.LocalDate;

public class Magazynier extends Fizyczny {
    public Magazynier(String imie_i_nazwisko, LocalDate dataUrodzenia, LocalDate dataZatrudnienia) {
        super(imie_i_nazwisko, dataUrodzenia, dataZatrudnienia);
        setStanowisko("Magazynier");
    }

    public double getWynagrodzenie(){
        return Wynagrodzenie * 0.8;
    }
}
