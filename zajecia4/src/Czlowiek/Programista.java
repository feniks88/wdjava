package Czlowiek;

import java.time.LocalDate;

public class Programista extends Umyslowy {
    public Programista(String imie_i_nazwisko, LocalDate dataUrodzenia, LocalDate dataZatrudnienia) {
        super(imie_i_nazwisko, dataUrodzenia, dataZatrudnienia);
        setStanowisko("Programista");
    }
    public double getWynagrodzenie(){
        return Wynagrodzenie * 1.7;
    }
}
