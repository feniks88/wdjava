package Czlowiek;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MainClass {
    public static void main(String[] args) {
        List<Pracownik> pracownicy = new ArrayList<>();
        pracownicy.add(new Ksiegowy("Jan Kowalski", LocalDate.of(1980,12,2), LocalDate.of(2018,1,1)));
        pracownicy.add(new Magazynier("Andrzej Front", LocalDate.of(1970,12,2), LocalDate.of(2018,1,1)));
        pracownicy.add(new Programista("Stefan Kowalski", LocalDate.of(1980,12,2), LocalDate.of(2018,1,1)));
        pracownicy.add(new Operator("Ola Kowalski", LocalDate.of(1980,12,2), LocalDate.of(2018,1,1)));

        for(Pracownik p: pracownicy){
            System.out.println(p.toString());
        }


    }
}
