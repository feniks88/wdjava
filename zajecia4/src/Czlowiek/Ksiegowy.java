package Czlowiek;

import java.time.LocalDate;

public class Ksiegowy extends Umyslowy {


    public Ksiegowy(String imie_i_nazwisko, LocalDate dataUrodzenia, LocalDate dataZatrudnienia) {
        super(imie_i_nazwisko, dataUrodzenia, dataZatrudnienia);
        setStanowisko("Ksiegowy");
    }

    public double getWynagrodzenie(){
        return Wynagrodzenie * 1.2;
    }
}
