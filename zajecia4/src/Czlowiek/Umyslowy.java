package Czlowiek;

import java.time.LocalDate;

public abstract class Umyslowy extends Pracownik {
    protected static final double Wynagrodzenie =  3000;
    public Umyslowy(String imie_i_nazwisko, LocalDate dataUrodzenia, LocalDate dataZatrudnienia) {
        super(imie_i_nazwisko, dataUrodzenia, dataZatrudnienia);
    }
    public abstract double getWynagrodzenie();
}
