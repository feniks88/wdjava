package Czlowiek;

import java.time.LocalDate;

public abstract class Czlowiek {
    private String imie_i_nazwisko;
    private LocalDate dataUrodzenia;



    public Czlowiek(String imie_i_nazwisko, LocalDate dataUrodzenia) {
        this.imie_i_nazwisko = imie_i_nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    public String getImie_i_nazwisko(){
        return imie_i_nazwisko;
    }

    public String getDataUrodzenia(){
        return dataUrodzenia.toString();
    }
}

