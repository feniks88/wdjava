package Czlowiek;

import java.time.LocalDate;

public class Operator extends Fizyczny {
    public Operator(String imie_i_nazwisko, LocalDate dataUrodzenia, LocalDate dataZatrudnienia) {
        super(imie_i_nazwisko, dataUrodzenia, dataZatrudnienia);
        setStanowisko("Operator");
    }

    public double getWynagrodzenie(){
        return Wynagrodzenie;
    }
}
