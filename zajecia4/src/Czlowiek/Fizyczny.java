package Czlowiek;

import java.time.LocalDate;

public abstract class Fizyczny extends Pracownik {

    protected static final double Wynagrodzenie =  3000;


    public abstract double getWynagrodzenie();

    public Fizyczny(String imie_i_nazwisko, LocalDate dataUrodzenia, LocalDate dataZatrudnienia) {
        super(imie_i_nazwisko, dataUrodzenia, dataZatrudnienia);
    }
}
