package Czlowiek;

import java.time.LocalDate;

public abstract class Pracownik extends Czlowiek {
    public abstract double getWynagrodzenie();
    private LocalDate dataZatrudnienia;
    private String stanowisko;
    public Pracownik(String imie_i_nazwisko, LocalDate dataUrodzenia, LocalDate dataZatrudnienia) {
        super(imie_i_nazwisko, dataUrodzenia);
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public String getStanowisko(){
        return stanowisko;
    }

    public void setStanowisko(String stanowisko){
        this.stanowisko = stanowisko;

    }

    public String getDataZatrudnienia(){
        return dataZatrudnienia.toString();
    }

    @Override
    public String toString(){
        return "Imie i nazwisko: " + getImie_i_nazwisko() + " Data zatrudnienia: " + getDataZatrudnienia() + " Data urodzenia: " + getDataUrodzenia() + " Stanowisko: " + getStanowisko() + " Wynagrodzenie: " + getWynagrodzenie();
    }
}
